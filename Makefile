CFLAGS = -I include -O3 -march=native
BIN = bin/main

${BIN}: obj/main.o obj/simple.o obj/fcts.o
	${CC} -o $@ $^

obj/main.o: src/main.c
	${CC} ${CFLAGS} -c -o $@ $<

obj/%.o: src/%.c include/%.h
	${CC} ${CFLAGS} -c -o $@ $<

.PHONY: clean run

clean:
	rm ./obj/*.o ./bin/*	

run: bin/main
	$<